# PrefTest #

Testing concurrent committing to SharedPreferences

1. Set amount of threads
2. Start test by 'START TEST'
3. Terminate app by 'CRASH ME NOW'
4. Restart app and see what thread has lost it value: CountX: 0
