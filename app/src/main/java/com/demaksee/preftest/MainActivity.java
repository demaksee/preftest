package com.demaksee.preftest;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

	private static final String COUNT_KEY = "count";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
		final PrefTest prefTest = new PrefTest(pref);

		int count = pref.getInt(COUNT_KEY, 2);

		ViewGroup container = (ViewGroup) findViewById(R.id.container);
		container.removeAllViews();
		for (int i = 0; i < count; i++) {

			TextView textView = new TextView(this);
			textView.setText("Count" + i + ": " + prefTest.getCountForThread(i));
			container.addView(textView);
		}

		final EditText editText = (EditText) findViewById(R.id.editTextCount);
		editText.setText("" + count);

		findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				int count = Integer.parseInt(editText.getText().toString());
				pref.edit().clear().commit();
				pref.edit().putInt(COUNT_KEY, count).commit();

				prefTest.startTest(count);
				Toast.makeText(MainActivity.this, "Finished, now restart!", Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.buttonCrash).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				restartApp();
			}
		});
	}

	private void restartApp() {
		//Thread.setDefaultUncaughtExceptionHandler(null);
		System.exit(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
