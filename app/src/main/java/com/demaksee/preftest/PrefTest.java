package com.demaksee.preftest;

import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by demaksee on 5/15/15.
 */
public class PrefTest {

	private SharedPreferences sharedPreferences;

	private CountDownLatch lineUpLatch;

	private CountDownLatch commitLatch = new CountDownLatch(1);

	public PrefTest(SharedPreferences sharedPreferences) {
		this.sharedPreferences = sharedPreferences;
	}

	public void startTest(int count){
		try {
			lineUpLatch = new CountDownLatch(count);

			ExecutorService executorService = Executors.newFixedThreadPool(count);

			for (int i = 0; i < count; i++) {
				executorService.submit(new FeedRunnable(String.valueOf(i) + "_", 1));
			}

			lineUpLatch.await();

			SystemClock.sleep(500);

			commitLatch.countDown();

			executorService.shutdown();

			executorService.awaitTermination(0, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public int getCountForThread(int i) {
		return getCountOfKeys(String.valueOf(i) + "_");
	}

	private int getCountOfKeys(String prefix) {
		int count = 0;
		for (String s : sharedPreferences.getAll().keySet()) {
			if (s.startsWith(prefix)) {
				count++;
			}
		}
		return count;
	}

	private class FeedRunnable implements Runnable {

		private static final String TAG = "FeedRunnable";

		private final String prefix;

		private final int count;

		public FeedRunnable(String prefix, int count) {

			this.prefix = prefix;
			this.count = count;
		}

		@Override
		public void run() {
			try {
				SharedPreferences.Editor edit = sharedPreferences.edit();
				for (int i = 0; i < count; i++) {
					String key = prefix + i;
					edit.putString(key, key);
				}

				lineUpLatch.countDown();

				commitLatch.await();

				Log.d(TAG, prefix + " - before commit ");
				edit.commit();
				Log.d(TAG, prefix + " - after commit ");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
